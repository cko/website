---
title: Termine
menu:
  after:
    weight: 20
---

# Termine
Hier findet ihr die Termine der nächsten Open-Source-Treffen. Sie finden im [Café Netzwerk](https://cafe-netzwerk.de) [(Schertlinstr. 4, 81379 München)](https://www.openstreetmap.org/node/9785880853#map=15/48.0984/11.5270) statt.

Das Café Netzwerk bietet günstige alkoholfreie Getränke sowie Pizzen und Snacks zum kleinen Preis.

## Monatliche Treffen
Diese finden von 19:00 bis ca. 22:00 Uhr statt. Die Teilnahme ist kostenlos, eine Anmeldung ist nicht erforderlich. Die Treffen stehen unter dem Motto "Jeder vierte Freitag im Monat für freie Software":

{{< hint info >}}
**Nächsten Termine:**
{{< /hint >}}
{{< hint info >}}
 - **27.10.2023 19:00 CEST** Christine Koppelt, Erika Lorenz-Löblein: Austausch zu Kinderbüchern über Open Source, Datenschutz, Internet, Software, etc.
{{< /hint >}}
{{< hint info >}}
 - **24.11.2023 19:00 CEST** Martin Zehetmayer: Supply Chain und SBOMs - Was ist das und wozu brauchen wir das?
{{< /hint >}}

## Workshops
{{< hint warning >}}
Derzeit sind keine Workshops geplant
{{< /hint >}}
