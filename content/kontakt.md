---
title: Kontakt
menu:
  after:
    weight: 60
---

# Kontakt

Für Anmeldungen, Anregungen und Feedback zum Open-Source-Treffen, schreibt einfach an

<a href="mailto:info@opensourcetreffen.de">info@opensourcetreffen.de</a>

Auch über Meldungen von Sponsoren, Unterstützern und Projekten, die mitmachen wollen, freuen wir uns immer!
