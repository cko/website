---
title: Start
menu:
  after:
    weight: 1
---

# Start

**Herzlich willkommen beim Open-Source-Treffen!**


Die Münchener Open-Source-Treffen bringen Projekte mit ihren Nutzern zusammen und fördern den Kontakt zwischen den Communities. Zusätzlich veranstalten wir regelmäßig Workshops und treffen uns zum Kochen oder zum Weißwurstfrühstück. Auf diesen Seiten erfahrt ihr alles über die Veranstaltungen und könnt euch Präsentationen der vergangenen Treffen herunterladen.

[Café Netzwerk](https://cafe-netzwerk.de) [(Schertlinstr. 4, 81379 München)](https://www.openstreetmap.org/node/9785880853#map=15/48.0984/11.5270), U-Bahn Haltestelle Machtlfingerstr. (U3)


{{< hint info >}}
Das nächste Treffen findet am **27.10.2023** statt (Siehe [Termine](/termine))
{{< /hint >}}


## Matrix

{{< hint info >}}
Wir nutzen mittlerweile **Matrix** als Hauptkommunikationsmittel.

Es gibt auf matrix.org einen öffentlichen Kanal: https://matrix.to/#/#open-source-treffen:matrix.org

Wer keine Informationen mehr verpassen möchte, ist herzlich dazu eingeladen beizutreten.
{{< /hint >}}
