---
title: Mailinglisten & Austausch
menu:
  after:
    weight: 40
---

# Austausch

Um mit euch in Kontakt zu bleiben, gibt es neben dieser Webseite auch Mailinglisten und Gruppen in sozialen Netzwerken.

## Matrix
Wir nutzen mittlerweile **Matrix** als Hauptkommunikationsmittel.

Es gibt auf matrix.org einen öffentlichen Kanal: https://matrix.to/#/#open-source-treffen:matrix.org

Wer keine Informationen mehr verpassen möchte, ist herzlich dazu eingeladen beizutreten.


## Mailinglisten

{{< hint danger >}}
Die Mailingliste ist derzeit außer Betrieb!
{{< /hint >}}

Jedem, der sich über Neuigkeiten bei den Open-Source-Treffen und -Workshops informieren will, empfehlen wir unsere [Announcement-Mailingliste](https://lists.opensourcetreffen.de/mailman/listinfo/announce). Mit höchstens zwei bis drei E-Mails im Monat halten wir euch über die aktuellen Entwicklungen, Termine und wichtige Änderungen auf dem Laufenden. Die Mailingliste ist quasi "Pflichtlektüre", da wir auch kurzfristige Terminänderungen dort veröffentlichen.

Ankündigungen rund um alle nicht-technischen Veranstaltungen wie unser Weißwurstfrühstück, das Kochen oder die Sauna, schicken wir ab sofort nur noch an unsere [Discuss-Mailingliste](https://lists.opensourcetreffen.de/mailman/listinfo/discuss), die zudem für Diskussionen der Teilnehmer untereinander gedacht ist.

**Zusammengefasst**: Aktuelles und Termine zu den Open-Source-Treffen und Open-Source-Workshops gibts nur auf der Announce-Liste, Aktuelles und Termine zu allen anderen Veranstaltungen, sowie Diskussion, gibts nur auf der Discuss-Liste.


## Soziale Netzwerke
Bei Facebook gibt es eine Fanseite für das [Open-Source-Treffen](https://www.facebook.com/opensourcetreffen).
