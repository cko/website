---
title: Danksagung
menu:
  after:
    weight: 80
---

# Danksagung

Wir möchten uns ganz herzlich bei folgenden Personen und Einrichtungen für ihre Beiträge und ihre Unterstützung rund um die Open-Source-Treffen bedanken!

- Dem gesamten Team vom [Café Netzwerk in München](https://www.cafe-netzwerk.de/), ohne deren Engagement die Veranstaltung in dieser Form nicht stattfinden könnte.
- [Anke Arnold](https://www.anke-art.de/), deren Schriftart "Fright Night" die Grundlage für unser Logo ist.
- Benjamin Bois, der uns eigens ein Möwen-Maskottchen gezeichnet hat.
- [Jens Habermann](https://www.jenshabermann.de/), der uns zahlreiche Grafiken erstellt und bearbeitet.
- [Hugo](https://github.com/gohugoio/hugo) und [Hugo-Book](https://github.com/alex-shpak/hugo-book) für die nützlichen Tools der Webseite
- Sowie natürlich allen Unterstützern, Referenten, Spendern und Gästen, die ihren Beitrag zum Erfolg der Open-Source-Treffen leisten.

**Herzlichen Dank!**
